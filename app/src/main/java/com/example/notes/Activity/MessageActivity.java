package com.example.notes.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.example.notes.Adapter.MessageAdapter;
import com.example.notes.Base.BaseActivity;
import com.example.notes.Main2Activity;
import com.example.notes.Model.Chat;
import com.example.notes.Model.User;
import com.example.notes.R;
import com.example.notes.Utils.Generalutils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MessageActivity extends BaseActivity {

    @BindView(R.id.profileimage)
    CircleImageView profileimage;

    @BindView(R.id.txtUsername)
    TextView txtUsername;

    @BindView(R.id.rcl_message)
    RecyclerView recyclerView;

    @BindView(R.id.edt_send)
    EditText edt_send;

    @BindView(R.id.activity)
    RelativeLayout activity;

    FirebaseStorage storage;
    StorageReference storageReference;

    @OnClick(R.id.btn_send)
    public void onSend() {
        String msg = edt_send.getText().toString();
        if(!msg.equals("")){
            sendMessage(mUser.getUid(),userid,msg);
            edt_send.setText("");
        }
        else {
            Generalutils.showSnackbar(activity,"Vui lòng nhập tin nhắn",null);
        }
    }

    String userid;

    FirebaseUser mUser;

    DatabaseReference mReference;

    List<Chat> mchat = new ArrayList<>();

    MessageAdapter adapter;

    @OnClick(R.id.btnBack)
    public void onBack() {
        startActivity(new Intent(MessageActivity.this, Main2Activity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }

    @Override
    protected void createView() {
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        Intent intent = this.getIntent();
        userid = intent.getStringExtra("userid");
        mReference = FirebaseDatabase.getInstance().getReference("Users").child(userid);
        getAvatar(userid,profileimage);
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                txtUsername.setText(user.getUsername());
                readMessage(mUser.getUid(), userid, user.getImageURL());
            }

            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {

            }
        });

    }

    private void sendMessage(String sender, String receiver, String message) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("sender", sender);
        hashMap.put("receiver", receiver);
        hashMap.put("message", message);
        reference.child("Chats").push().setValue(hashMap);

    }

    private void readMessage(final String myid, final String userid, final String imgurl) {
        mchat = new ArrayList<>();
        mReference = FirebaseDatabase.getInstance().getReference("Chats");
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mchat.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Chat chat = snapshot.getValue(Chat.class);
                    if (chat.getReceiver().equals(myid) && chat.getSender().equals(userid) ||
                            chat.getReceiver().equals(userid) && chat.getSender().equals(myid)) {
                        mchat.add(chat);
                    }
                    adapter = new MessageAdapter(MessageActivity.this, mchat);
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getAvatar(String id, final CircleImageView view){
        StorageReference ref = storageReference.child("images/"+ id);
        ref.getDownloadUrl().addOnSuccessListener(
                new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(final Uri uri) {
                        Picasso.get().load(uri).into(view);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull final Exception e) {
                if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
                    view.setImageDrawable(getDrawable(R.drawable.ic_person_black_24dp));
                }
            }
        });
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_message;
    }
}
