package com.example.notes.Base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    protected abstract void createView();

    protected abstract int getLayoutID();

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutID());
        ButterKnife.bind(this);
        createView();
    }
}
