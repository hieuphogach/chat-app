package com.example.notes;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import com.example.notes.Base.BaseActivity;
import com.example.notes.Utils.Generalutils;
import com.example.notes.Utils.ProgressDialogUtil;
import com.example.notes.Utils.SimpleTextWatcher;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotpassActivity extends BaseActivity {

    private FirebaseAuth auth;

    @BindView(R.id.txtEmail)
    TextInputLayout txtEmail;

    @BindView(R.id.edtEmail)
    EditText edtEmail;

    @BindView(R.id.activity)
    ConstraintLayout main_content;


    @OnClick(R.id.btnConfirm)
    public void onForgot(){
        final ProgressDialogUtil progressDialogUtil = new ProgressDialogUtil(this);
        if(TextUtils.isEmpty(txtEmail.getError())){
            progressDialogUtil.show(false);
            auth.sendPasswordResetEmail(edtEmail.getText().toString()).addOnCompleteListener(
                    new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull final Task<Void> task) {
                            if(task.isSuccessful()) {
                                progressDialogUtil.hideProgress();
                                Generalutils.showSnackbar(main_content, getString(R.string.emailsent), null);
                            }
                            else{
                                progressDialogUtil.hideProgress();
                                Generalutils.showSnackbar(main_content, getString(R.string.error), getString(R.string.retry));
                            }
                        }
                    });

        }
        else{
            Generalutils.showSnackbar(main_content,getString(R.string.requiredfield),getString(R.string.retry));
        }
    }

    @OnClick(R.id.btnBack)
    public void onBack(){
        onBackPressed();
    }

    @Override
    protected void createView() {
        auth = FirebaseAuth.getInstance();
        edtEmail.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
                if(s.length()==0){
                    txtEmail.setError(getString(R.string.requiredfield));
                }
                else if(!Generalutils.isValidEmailId(s.toString())){
                    txtEmail.setError(getString(R.string.wrongemail));
                }
                else {
                    txtEmail.setError(null);
                }
            }
        });
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_forgotpass;
    }

}
