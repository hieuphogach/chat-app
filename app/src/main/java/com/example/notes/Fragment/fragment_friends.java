package com.example.notes.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import butterknife.BindView;
import com.example.notes.Adapter.UserAdapter;
import com.example.notes.Base.BaseFragment;
import com.example.notes.LoginActivity;
import com.example.notes.Model.User;
import com.example.notes.R;
import com.example.notes.RegisterActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;

public class fragment_friends extends BaseFragment {
    @BindView(R.id.rclUser)
    RecyclerView rclUser;
    UserAdapter adapter;

    private List<User> arrUser = new ArrayList<>();
    @Override
    protected int getLayoutId() {
        return R.layout.layout_fragmentlogin;
    }

    @Override
    protected void initView(final View view) {
        rclUser.setHasFixedSize(true);
        rclUser.setLayoutManager(new LinearLayoutManager(getContext()));
        readUsers();
    }

    private void readUsers() {
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    arrUser.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        User user = snapshot.getValue(User.class);
                        assert user != null;
                        assert firebaseUser != null;
                        if (!user.getId().equals(firebaseUser.getUid())) {
                            arrUser.add(user);
                        }
                    }
                    adapter = new UserAdapter(getContext(), arrUser);
                    rclUser.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
