package com.example.notes.Fragment;

import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import com.example.notes.Adapter.ChatAdapter;
import com.example.notes.Base.BaseFragment;
import com.example.notes.Model.Chat;
import com.example.notes.Model.User;
import com.example.notes.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
import java.util.ArrayList;
import java.util.List;

public class fragment_home extends BaseFragment {
    @BindView(R.id.profileimage)
    CircleImageView profileimage;

    @BindView(R.id.txtUsername)
    TextView username;

    @BindView(R.id.rcl_message)
    RecyclerView recyclerView;
    List<User> mUsers;
    ChatAdapter adapter;
    StorageReference storageReference;
    List<String> usersList = new ArrayList<>();
    FirebaseUser mUser;
    FirebaseStorage storage;
    DatabaseReference reference;

    protected int getLayoutId() {
        return R.layout.layout_fragmenthome;
    }

    @Override
    protected void initView(final View view) {
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        getUsername();
        getAvatar();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            //lay ra user co tin nhan voi minh
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usersList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Chat chat = snapshot.getValue(Chat.class);
                    if(chat.getSender().equals(mUser.getUid())){
                        usersList.add(chat.getReceiver());
                    }
                    if(chat.getReceiver().equals(mUser.getUid())){
                        usersList.add(chat.getSender());
                    }
                }
                readUsers();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void readUsers() {
        mUsers = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference("Users");
        reference.addValueEventListener(new ValueEventListener() {
            //check trung va them user vao list
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mUsers.clear();
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    User user = snapshot.getValue(User.class);
                    for (String id : usersList){
                        if(user.getId().equals(id)){
                            if(mUsers.size()!=0){
                                for(User user1 : mUsers){
                                    if(!(user.getId().equals(user1.getId()))){
                                        mUsers.add(user);
                                    }
                                }
                            }
                            else{
                                mUsers.add(user);
                            }
                        }
                    }
                }
                adapter = new ChatAdapter(getContext(),mUsers);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getUsername(){
        reference = FirebaseDatabase.getInstance().getReference("Users").child(mUser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                username.setText(user.getUsername());
            }

            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {

            }
        });
    }

    private void getAvatar(){
        StorageReference ref = storageReference.child("images/"+ mUser.getUid());
        ref.getDownloadUrl().addOnSuccessListener(
                new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(final Uri uri) {
                        Picasso.get().load(uri).into(profileimage);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull final Exception e) {
                if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
                    profileimage.setImageDrawable(getActivity().getDrawable(R.drawable.ic_person_black_24dp));
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
