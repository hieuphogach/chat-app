package com.example.notes.Fragment;

import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.Manifest.permission;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.example.notes.Base.BaseFragment;
import com.example.notes.LoginActivity;
import com.example.notes.Model.User;
import com.example.notes.R;
import com.example.notes.Utils.Generalutils;
import com.example.notes.Utils.ProgressDialogUtil;
import com.example.notes.Utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask.TaskSnapshot;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
import java.io.File;
import java.io.IOException;

public class fragment_user extends BaseFragment {

    private DatabaseReference mDatabaseReference;

    private FirebaseAuth mAuth;

    private FirebaseUser mUser;

    @BindView(R.id.txtUsername)
    TextView txtUsername;

    @BindView(R.id.txtEmail)
    TextView txtEmail;

    @OnClick(R.id.edtPassword)
    public void onChangepass() {
        AlertDialog alertbox = new Builder(getContext())
                .setMessage("Một mail sẽ được gửi tới hòm thư của bạn. Bạn có muốn tiếp tục không?")
                .setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (Generalutils.isNetworkAvailable(getContext())) {
                            mAuth.sendPasswordResetEmail(mUser.getEmail()).addOnCompleteListener(
                                    new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull final Task<Void> task) {
                                            if(task.isSuccessful()) {
                                                Generalutils.showSnackbar(main_content, getString(R.string.emailsent),
                                                        null);
                                            }
                                            else{
                                                Generalutils.showSnackbar(main_content, getString(R.string.wrongemail),
                                                        null);
                                            }
                                        }
                                    });
                        } else {
                            Generalutils.showSnackbar(main_content, getString(R.string.unablenetwork),
                                    null);
                        }
                    }
                })
                .setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();
    }

    @BindView(R.id.txtUID)
    TextView txtUID;

    @BindView(R.id.activity)
    ConstraintLayout main_content;

    private File file = null;

    FirebaseStorage storage;

    StorageReference storageReference;

    DatabaseReference mReference;

    @OnClick(R.id.btnLogout)
    public void onLogout() {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @OnClick(R.id.btnChangepass)
    public void onChangePass() {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("notes");

    }

    SharedPreferences sharedPreferences;

    @BindView(R.id.avatar)
    CircleImageView avatar;


    public void onUpload() {
        if (ContextCompat.checkSelfPermission(getContext(),
                permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);
            }
        } else {
            showGallery();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_fragmentuser;
    }

    @Override
    protected void initView(final View view) {
        mAuth = FirebaseAuth.getInstance();
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        sharedPreferences = getActivity().getSharedPreferences(Utils.prefname, Context.MODE_PRIVATE);
        txtEmail.setText(mUser.getEmail());
        txtUID.setText(mUser.getUid());
        StorageReference ref = storageReference.child("images/" + mUser.getUid());
        ref.getDownloadUrl().addOnSuccessListener(
                new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(final Uri uri) {
                        Picasso.get().load(uri).into(avatar);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull final Exception e) {
                if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
                    avatar.setImageDrawable(getActivity().getDrawable(R.drawable.ic_person_black_24dp));
                }
            }
        });
        mReference = FirebaseDatabase.getInstance().getReference("Users").child(mUser.getUid());
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                txtUsername.setText(user.getUsername());
            }

            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {

            }
        });

        avatar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                onUpload();
            }
        });
    }

    private void showGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Chọn avatar"), 1);
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            final Uri filepath = data.getData();
            final ProgressDialogUtil progressDialogUtil = new ProgressDialogUtil(getContext());
            if (filepath != null) {
                progressDialogUtil.show(false);
                StorageReference ref = storageReference.child("images/" + mUser.getUid());
                ref.putFile(filepath).addOnSuccessListener(new OnSuccessListener<TaskSnapshot>() {
                    @Override
                    public void onSuccess(final TaskSnapshot taskSnapshot) {
                        progressDialogUtil.hideProgress();
                        Generalutils.showSnackbar(main_content, getString(R.string.uploadsuccess), null);
                        try {
                            Bitmap bitmap = MediaStore.Images.Media
                                    .getBitmap(getActivity().getContentResolver(), filepath);
                            avatar.setImageBitmap(bitmap);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull final Exception e) {
                        progressDialogUtil.hideProgress();
                        Generalutils.showSnackbar(main_content, getString(R.string.uploadfail), null);
                    }
                });
            }

        }
    }

    public void onRequestPermissionsResult(int requestCode,
            String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    showGallery();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
