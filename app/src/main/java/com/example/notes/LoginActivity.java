package com.example.notes;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.OnClick;
import com.example.notes.Base.BaseActivity;
import com.example.notes.Utils.Generalutils;
import com.example.notes.Utils.ProgressDialogUtil;
import com.example.notes.Utils.SimpleTextWatcher;
import com.example.notes.Utils.StorePreference;
import com.example.notes.Utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends BaseActivity {
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    @BindView(R.id.activity)
    ConstraintLayout main_content;

    @BindView(R.id.txtUser)
    TextInputLayout txtUser;

    @BindView(R.id.txtPassword)
    TextInputLayout txtPass;

    @BindView(R.id.edtUser)
    EditText edtuser;

    @BindView(R.id.edtPassword)
    EditText edtPass;

    @OnClick(R.id.txtForgot)
    public void onForgot(){
        Intent intent = new Intent(this,ForgotpassActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnLogin)
    public void onLogin(){
        if(Generalutils.isNetworkAvailable(this)){
            final ProgressDialogUtil progressDialogUtil = new ProgressDialogUtil(this);
            if(TextUtils.isEmpty(txtUser.getError()) && TextUtils.isEmpty(txtPass.getError()) && checkEmpty()){
                progressDialogUtil.show(false);
                mAuth.signInWithEmailAndPassword(edtuser.getText().toString(), edtPass.getText().toString())
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    Log.d(Utils.TAG, "createUserWithEmail:success");
                                    progressDialogUtil.hideProgress();
                                    Generalutils.showSnackbar(main_content,getString(R.string.loginsuccess),null);
                                    Intent intent = new Intent(LoginActivity.this,Main2Activity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                } else {
                                    // If sign in fails, display a message to the user.
                                    Log.w(Utils.TAG, "createUserWithEmail:failure", task.getException());
                                    progressDialogUtil.hideProgress();
                                    Generalutils.showSnackbar(main_content,getString(R.string.wronguserpass),null);
                                }
                            }
                        });
            }
            else{
                Generalutils.showSnackbar(main_content,getString(R.string.warningcompletefield),null);
            }
        }
        else {
            Generalutils.showSnackbar(main_content,getString(R.string.unablenetwork),getString(R.string.retry));
        }
    }


    @OnClick(R.id.btnRegister)
    public void onRegister(){
        Intent intent = new Intent(this,RegisterActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_in, R.anim.anim_out);

    }

    @Override
    protected void createView() {
        mAuth = FirebaseAuth.getInstance();


        edtuser.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
                if(s.length()==0){
                    txtUser.setError(getString(R.string.requiredfield));
                }
                else if(!Generalutils.isValidEmailId(s.toString())){
                    txtUser.setError(getString(R.string.wrongemail));
                }
                else{
                    txtUser.setError(null);
                }
            }
        });
        edtPass.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
                if(s.length()==0){
                    txtPass.setError(getString(R.string.requiredfield));
                }
                else if(!Generalutils.isValidPass(s.toString())){
                    txtPass.setError(getString(R.string.wrongpass));
                }
                else {
                    txtPass.setError(null);
                }
            }
        });
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_login;
    }

    private boolean checkEmpty(){
        if(TextUtils.isEmpty(edtuser.getText().toString()) && TextUtils.isEmpty(edtPass.getText().toString())){
            return false;
        }
        else return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        if(mUser!=null){
            Intent intent = new Intent(this,Main2Activity.class);
            startActivity(intent);
        }
    }
}
