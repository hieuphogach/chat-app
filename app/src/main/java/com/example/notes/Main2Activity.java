package com.example.notes;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import butterknife.BindView;
import com.example.notes.Base.BaseActivity;
import com.example.notes.Fragment.fragment_home;
import com.example.notes.Fragment.fragment_friends;
import com.example.notes.Fragment.fragment_user;
import com.example.notes.Utils.Utils;

public class Main2Activity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener{


    @BindView(R.id.navigation)
    BottomNavigationView navigation;
    SharedPreferences sharedPreferences;
    private boolean isLogged;

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.navigation_home:
                fragment = new fragment_home();
                break;

            case R.id.navigation_dashboard:
                fragment = new fragment_friends();
                break;
            case R.id.navigation_profile:
                fragment = new fragment_user();
                break;
        }

        return loadFragment(fragment);
    }

    @Override
    protected void createView() {
        sharedPreferences = getSharedPreferences(Utils.prefname, Context.MODE_PRIVATE);
        isLogged = sharedPreferences.getBoolean(Utils.isLogged,false);
        navigation.setOnNavigationItemSelectedListener(this);
        loadFragment(new fragment_home());

    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_main2;
    }


    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

}
