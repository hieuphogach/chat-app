package com.example.notes.Model;

public class User {
    private String id;
    private String imageURL;
    private String username;

    public User(){};

    public User(final String username, final String imageURL, final String id) {
        this.id = id;
        this.username = username;
        this.imageURL = imageURL;
    }

    public String getId() {
        return id;
    }

    public void setId(final String uid) {
        this.id = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(final String imageURL) {
        this.imageURL = imageURL;
    }
}
