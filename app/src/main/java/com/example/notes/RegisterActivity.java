package com.example.notes;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.OnClick;
import com.example.notes.Base.BaseActivity;
import com.example.notes.Utils.Generalutils;
import com.example.notes.Utils.ProgressDialogUtil;
import com.example.notes.Utils.SimpleTextWatcher;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.HashMap;

public class RegisterActivity extends BaseActivity {

    private static final String TAG = "TAG";

    private FirebaseAuth mAuth;


    @BindView(R.id.activity)
    ConstraintLayout main_content;

    @BindView(R.id.txtUser)
    TextInputLayout txtUser;

    @BindView(R.id.txtPassword)
    TextInputLayout txtPass;

    @BindView(R.id.edtUser)
    EditText edtuser;

    @BindView(R.id.edtPassword)
    EditText edtPass;

    @BindView(R.id.txtRepeat)
    TextInputLayout txtRepeat;

    @BindView(R.id.edtRepeat)
    EditText edtRepeat;

    @BindView(R.id.txtUsername)
    TextInputLayout txtUsername;

    @BindView(R.id.edtUsername)
    EditText edtUsername;

    DatabaseReference mReference;

    @OnClick(R.id.btnBack)
    public void onBack() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
    }

    @OnClick(R.id.btnRegister)
    public void onRegister() {
        if (Generalutils.isNetworkAvailable(this)) {
            final ProgressDialogUtil progressDialogUtil = new ProgressDialogUtil(this);
            if (TextUtils.isEmpty(txtUser.getError()) && TextUtils.isEmpty(txtPass.getError()) && TextUtils
                    .isEmpty(txtRepeat.getError()) && TextUtils.isEmpty(txtUsername.getError()) && checkEmpty()) {
                progressDialogUtil.show(false);

                mAuth.createUserWithEmailAndPassword(edtuser.getText().toString(), edtPass.getText().toString())
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    String userid = user.getUid();
                                    mReference = FirebaseDatabase.getInstance().getReference("Users").child(userid);
                                    HashMap<String, String> hashMap = new HashMap<>();
                                    hashMap.put("id", userid);
                                    hashMap.put("username", edtUsername.getText().toString());
                                    hashMap.put("imageURL", "default");
                                    mReference.setValue(hashMap).addOnCompleteListener(
                                            new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull final Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        progressDialogUtil.hideProgress();
                                                        Generalutils.showSnackbar(main_content,
                                                                getString(R.string.registersuccess), null);
                                                        finish();
                                                    }
                                                }
                                            });
                                } else {
                                    // If sign in fails, display a message to the user.
                                    Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                    progressDialogUtil.hideProgress();
                                    Generalutils.showSnackbar(main_content, getString(R.string.authfailed),
                                            getString(R.string.retry));
                                }
                            }
                        });
            } else {
                Generalutils.showSnackbar(main_content, getString(R.string.warningcompletefield), null);
            }
        } else {
            Generalutils.showSnackbar(main_content, getString(R.string.unablenetwork), getString(R.string.retry));
        }
    }

    @Override
    protected void createView() {

        mAuth = FirebaseAuth.getInstance();

        edtuser.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
                if (s.length() == 0) {
                    txtUser.setError(getString(R.string.requiredfield));
                } else if (!Generalutils.isValidEmailId(s.toString())) {
                    txtUser.setError(getString(R.string.wrongemail));
                } else {
                    txtUser.setError(null);
                }
            }
        });
        edtPass.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
                if (s.length() == 0) {
                    txtPass.setError(getString(R.string.requiredfield));
                } else if (!Generalutils.isValidPass(s.toString())) {
                    txtPass.setError(getString(R.string.wrongpass));
                } else {
                    txtPass.setError(null);
                }
            }
        });

        edtRepeat.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
                if (s.length() == 0) {
                    txtRepeat.setError(getString(R.string.requiredfield));
                } else if (!s.toString().equals(edtPass.getText().toString())) {
                    txtRepeat.setError(getString(R.string.notequal));
                } else {
                    txtRepeat.setError(null);
                }
            }
        });
        edtUsername.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
                if (s.length() == 0) {
                    txtPass.setError(getString(R.string.requiredfield));
                } else if (!Generalutils.isValidPass(s.toString())) {
                    txtPass.setError(getString(R.string.wrongpass));
                } else {
                    txtPass.setError(null);
                }
            }
        });
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_register;
    }

    private boolean checkEmpty() {
        if (TextUtils.isEmpty(edtuser.getText().toString()) && TextUtils.isEmpty(edtPass.getText().toString())
                && TextUtils.isEmpty(edtUsername.getText().toString()) && TextUtils
                .isEmpty(edtRepeat.getText().toString())) {
            return false;
        } else {
            return true;
        }
    }

//    private void readUsers(final String username) {
//        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users");
//        databaseReference.addValueEventListener(new CustomValueListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
//                    User user = snapshot.getValue(User.class);
//                    assert user != null;
//                    assert firebaseUser != null;
//                    if (user.getUsername().equals(username)) {
//                        Toast.makeText(RegisterActivity.this,"Trung tai khoan",Toast.LENGTH_LONG).show();
//                    }
//                }
//            }
//        });
//    }
}
