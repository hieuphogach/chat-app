package com.example.notes.Utils;

import android.support.annotation.NonNull;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public abstract class CustomValueListener implements ValueEventListener {

    @Override
    public void onCancelled(@NonNull final DatabaseError databaseError) {

    }
}
