package com.example.notes.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.View;
import com.example.notes.R;
import java.util.regex.Pattern;

public class Generalutils {
    public static void showSnackbar(View v, String message, String actiontext){
        if (actiontext!=null) {
            Snackbar snackbar = Snackbar
                    .make(v, message, Snackbar.LENGTH_LONG)
                    .setAction(actiontext, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //Code khi bấm vào nút thư lại ở đây
                        }
                    });
            snackbar.show();
        }
        else{
            Snackbar snackbar = Snackbar
                    .make(v, message, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public static boolean isValidEmailId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public static boolean isValidPass(String pass){
        if(pass.length()>6){
            return true;
        }
        else{
            return false;
        }
    }

    public static void showDialog(final Context context, String message){
        AlertDialog alertbox = new Builder(context)
                .setMessage(message)
                .setPositiveButton(R.string.Yes, new OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                        ((Activity)context).finish();
                    }
                })
                .setNegativeButton(R.string.No, new OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();
    }

}
