package com.example.notes.Utils;

import static android.content.Context.MODE_PRIVATE;
import android.content.Context;
import android.content.SharedPreferences;
import com.google.firebase.auth.FirebaseUser;

public class StorePreference {
    public static void savingPreferences(Context context, boolean isLogged, String email)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences
                (Utils.prefname, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Utils.isLogged,isLogged);
        editor.putString(Utils.email,email);
        editor.apply();
    }

    public static void savingPrefByUser(Context context, boolean isLogged, FirebaseUser user){
        SharedPreferences sharedPreferences=context.getSharedPreferences
                (Utils.prefname, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Utils.isLogged,isLogged);
        editor.putString(Utils.email,user.getEmail());
        editor.putString(Utils.DisplayName,user.getDisplayName());
        editor.putString(Utils.phonenumber,user.getPhoneNumber());
//        editor.putString(Utils.PhotoUrl,user.getPhotoUrl().toString());
        editor.putString(Utils.UID,user.getUid());
        editor.apply();
    }


}
